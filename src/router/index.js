import Vue from 'vue'
import Router from 'vue-router'
import Form from '@/pages/Form'
import AnotherPage from '@/pages/AnotherPage'
import YetAnotherPage from '@/pages/yetAnotherPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Form',
      component: Form
    },
    {
      path: '/another',
      name: 'Something Other',
      component: AnotherPage,
      meta: {
        layout: 'SplitPane'
      }
    },
    {
      path: '/yetAnother',
      name: 'Hell Is Others',
      component: YetAnotherPage,
      meta: {
        tab: 'settings'
      }
    }
  ]
})
